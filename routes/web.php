<?php

use Cadix\LaravelZoom\Middleware\Webhook;
use Illuminate\Support\Facades\Route;

Route::group(['prefix' => 'zoom', 'as' => 'zoom.'], static function () {
    Route::post('webhooks', \Cadix\LaravelZoom\Http\Controllers\WebhookController::class)
        ->middleware(Webhook::class)
        ->name('webhooks.handle');
});
