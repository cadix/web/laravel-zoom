![banner](https://banners.beyondco.de/Laravel%20Zoom.png?theme=light&packageName=cadix%2Flaravel-zoom&pattern=cage&style=style_1&description=A+Laravel+wrapper+package+for+Zoom+API&md=1&showWatermark=1&fontSize=200px&images=link)

# Introduction

This package is meant for JWT actions with Zoom's REST API.

Icon provided by [Zoom](https://explore.zoom.us/docs/en-us/brandguidelines.html#mediaresources).

[![coverage report](https://gitlab.com/cadix/web/laravel-zoom/badges/master/coverage.svg?style=flat)](https://gitlab.com/cadix/web/laravel-zoom/-/commits/master)
[![pipeline status](https://gitlab.com/cadix/web/laravel-zoom/badges/master/pipeline.svg?style=flat)](https://gitlab.com/cadix/web/laravel-zoom/-/commits/master)

[[_TOC_]]

# Installation
            
Run the command below to install the package

```bash
composer require cadix/laravel-zoom
```

## Configuration
        
Running the command below to expose the configuration. Or override them in your `.env` file.
```bash
 php artisan vendor:publish --provider="Cadix\LaravelZoom\LaravelZoomServiceProvider" --tag="config"
```

# About Cadix
With a team of passionate professionals [Cadix](https://www.cadix.nl) delivers solutions for designing, managing and sharing of information in construction, mechanical engineering and infra.
We realize this with our trainings, consultancy and secondment.
With the extensive knowledge of our professionals we are able to find and realize a suitable solution for you.  

# References
- [API Documentation](https://marketplace.zoom.us/docs/guides);
- [API endpoints](https://marketplace.zoom.us/docs/api-reference/zoom-api/methods/);
- [Webhook events](https://marketplace.zoom.us/docs/api-reference/zoom-api/events/);