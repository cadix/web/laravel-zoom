<?php

namespace Cadix\LaravelZoom\Database\Factories;

use Cadix\LaravelZoom\Models\ZoomOAuth;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\Cadix\LaravelZoom\Models\ZoomOAuth>
 */
class ZoomOAuthFactory extends Factory
{
    protected $model = ZoomOAuth::class;

    public function definition(): array
    {
        return [
            'access_token' => Str::random(),
            'token_type'   => 'Bearer',
            'expires_at'   => now()->addHour()->toDateTimeString(),
        ];
    }
}
