<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateZoomOAuthsTable extends Migration
{
    public function up(): void
    {
        if (! Schema::hasTable('zoom_oauths')) {
            Schema::create('zoom_oauths', static function (Blueprint $table) {
                $table->id();
                $table->text('access_token');
                $table->string('token_type');
                $table->dateTime('expires_at');
            });
        }
    }

    public function down(): void
    {
        Schema::dropIfExists('zoom_oauths');
    }
}
