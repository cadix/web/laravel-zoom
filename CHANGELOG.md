# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/).

## [0.4.0]
### Added
- Added support for Laravel 11;
- Added support for PHP 8.3;

### Removed
- Removed support for Laravel 8 & 9;
- Removed support for PHP 8.0 & 8.1;

## [0.3.1]
### Fixed
- Fixed webhook middleware

## [0.3.0]
### Changed
- Moved from JWT to server-to-server OAuth (#1);
- Changed responses from array to object. This should mimic Laravel a bit more;

## [0.2.0]
### Added
- Added webhooks
### Changed
- Updated packages

## [0.1.2]
### Added
- Added support for Laravel 9

## [0.1.1]

## [0.1.0]
- Initial release