<?php

namespace Cadix\LaravelZoom\Facades;

use Cadix\LaravelZoom\Client as RootClient;
use Illuminate\Support\Facades\Facade;

/**
 * @mixin RootClient
 *
 * @method static array|null get()
 * @method static array|null post(array|null $body = null)
 * @method static array|null put(array $body)
 * @method static array|null delete()
 * @method static array|null request(string $method, string $url, array|null $headers = null, array $body = [], int $tries = 0)
 */
class Client extends Facade
{
    protected static function getFacadeAccessor(): string
    {
        return RootClient::class;
    }
}
