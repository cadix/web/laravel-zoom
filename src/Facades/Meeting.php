<?php

namespace Cadix\LaravelZoom\Facades;

use Cadix\LaravelZoom\Meeting as RootMeeting;
use Illuminate\Support\Facades\Facade;

/**
 * @mixin RootMeeting
 *
 * @method static object|null all(string $user_id)
 * @method static object|null get(string $user_id)
 * @method static object|null find(string $id)
 * @method static object|null create(string $user_id, array $body)
 * @method static object|null update(string $id, array $body)
 * @method static object|null delete(string $id, array $parameters = null)
 */
class Meeting extends Facade
{
    protected static function getFacadeAccessor(): string
    {
        return RootMeeting::class;
    }
}
