<?php

namespace Cadix\LaravelZoom\Facades;

use Cadix\LaravelZoom\User as RootUser;
use Illuminate\Support\Facades\Facade;

/**
 * @mixin RootUser
 *
 * @method static array|null  all()
 * @method static array|null  get()
 * @method static object|null find(string $id)
 * @method static object|null create(array $body)
 * @method static object|null update(string $id, array $body)
 * @method static object|null delete(string $id, array $parameters)
 */
class User extends Facade
{
    protected static function getFacadeAccessor(): string
    {
        return RootUser::class;
    }
}
