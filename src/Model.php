<?php

namespace Cadix\LaravelZoom;

use Cadix\LaravelZoom\Models\ZoomOAuth;
use Illuminate\Support\Facades\Http;

class Model
{
    private string $baseUrl;

    public function __construct(public Client $client)
    {
        $this->baseUrl = config('zoom.base_url');
    }

    public function paginate(int $perPage = 10, int $page = 1, int|string $user_id = null): object
    {
        $model = $this->getModel();

        if (! $this->client->url) {
            $this->client->url = $this->getBaseUrl().$model;
        }

        $this->client->params = [
            'page_size'   => $perPage,
            'page_number' => $page,
        ];
        $this->setHeaders();
        $result = $this->client->get();

        return (object) [
            'total'        => $result->total_records,
            'per_page'     => $perPage,
            'current_page' => $page,
            'last_page'    => (int) ceil($result->total_records / $perPage),
            'from'         => 1 === $page ? 1 : $page * $perPage,
            'to'           => 1 === $page ? $perPage : ($page * $perPage) + $perPage,
            'data'         => (array) $result->{$model},
        ];
    }

    public function destroy(string|int $id): bool
    {
        $this->setHeaders();

        return is_null($this->client->delete());
    }

    public function get(string|null $user_id = null): array|object|null
    {
        $this->setHeaders();

        return $this->client->get();
    }

    public function post(array|null $body = null): object|null
    {
        $this->setHeaders();

        return $this->client->post($body);
    }

    public function patch(array $body): object|null
    {
        $this->setHeaders();

        return $this->client->patch($body);
    }

    public function put(array $body): object|null
    {
        $this->setHeaders();

        return $this->client->put($body);
    }

    private function setHeaders(): void
    {
        $this->client->headers = [
            'Authorization'   => 'Bearer '.$this->getZoomAccessToken(),
            'Accept'          => 'application/json; charset=utf-8',
            'Accept-Language' => 'en',
            'Content-Type'    => 'application/json; charset=utf-8',
        ];
    }

    public function getHeaders(): array|null
    {
        $this->setHeaders();

        return $this->client->headers;
    }

    protected function getBaseUrl(): string
    {
        return $this->baseUrl;
    }

    public function getModel(): string
    {
        return $this->model ?? class_basename($this);
    }

    public function limit(int $limit): self
    {
        $this->client->params = [
            'page_size' => $limit,
        ];

        return $this;
    }

    private function getZoomAccessToken(): string
    {
        $oauth = ZoomOAuth::where('expires_at', '>', now()->toDateTimeString())->first();

        if ($oauth) {
            return $oauth->access_token;
        }

        $url = sprintf(
            'https://zoom.us/oauth/token?grant_type=account_credentials&account_id=%s',
            config('zoom.account_id'),
        );

        $authorization = sprintf(
            'Basic %s',
            base64_encode(config('zoom.client_id').':'.config('zoom.client_secret'))
        );

        $token_response = Http::withHeaders([
            'Authorization' => $authorization,
        ])
            ->post($url);

        if (! $token_response->successful()) {
            throw new \Exception('Something went wrong getting Zoom acces: '.$token_response->status());
        }
        $token_response = $token_response->object();

        ZoomOAuth::updateOrCreate(
            [
                'token_type' => 'Bearer',
            ],
            [
                'access_token' => $token_response->access_token,
                'expires_at'   => now()->addSeconds($token_response->expires_in)->toDateTimeString(),
            ]
        );

        return $token_response->access_token;
    }
}
