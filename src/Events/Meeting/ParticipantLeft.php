<?php

namespace Cadix\LaravelZoom\Events\Meeting;

use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Http\Request;
use Illuminate\Queue\SerializesModels;

class ParticipantLeft
{
    use Dispatchable;
    use SerializesModels;

    public object $body;

    public function __construct(Request $request)
    {
        $this->body = json_decode($request->getContent());
    }
}
