<?php

namespace Cadix\LaravelZoom\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class Webhook
{
    /**
     * Handle an incoming request.
     * https://marketplace.zoom.us/docs/api-reference/webhook-reference/#verify-webhook-events
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next): mixed
    {
        if ($request->hasHeader('x-zm-signature') && !$this->checkSignature($request)) {
            abort(403);
        }

        return $next($request);
    }

    private function checkSignature(Request $request): bool
    {
        $content = $request->getContent();
        $timestamp = $request->header('x-zm-request-timestamp');
        $message = sprintf(
            'v0:%s:%s',
            $timestamp,
            $content
        );

        $hashed_message = hash_hmac('sha256', $message, config('zoom.secret_token'));
        $signature = 'v0='.$hashed_message;

        return $signature === $request->header('x-zm-signature');
    }
}
