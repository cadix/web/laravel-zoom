<?php

namespace Cadix\LaravelZoom;

use Illuminate\Http\Client\Request;
use Illuminate\Support\Facades\Http;

trait ZoomApplication
{
    public function mockZoomRequest(array|null $requests = null): void
    {
        $responses = Http::sequence();

        if ($requests) {
            foreach ($requests as $request) {
                $content = null;
                // Get content by model and function
                if (empty($request['body']) && ! empty($request['model']) && ! empty($request['function'])) {
                    $content = $this->getZoomResponseContent($request['model'], $request['function']);
                }

                // Allow content from tests
                if (! empty($request['body']) && empty($request['model']) && empty($request['function'])) {
                    $content = $request['body'];
                }

                $responses->push(
                    $content,
                    $request['status'] ?? 200,
                    $request['headers'] ?? ['Content-Type' => 'application/json']
                );
            }
        }

        if (! $requests) {
            $responses->push(null, 200, ['Content-Type' => 'application/json']);
        }

        Http::fake([
            config('zoom.base_url').'*' => $responses,
        ]);
    }

    public function assertSentToZoom(array $assertions): void
    {
        foreach ($assertions as $assert) {
            Http::assertSent(static function (Request $request) use ($assert) {
                return str_contains($request->url(), config('zoom.base_url')) &&
                    str_contains($request->url(), $assert['function']) &&
                    $request->method() === ($assert['method'] ?? 'POST') &&
                    (! isset($assert['body']) || $request->body() === $assert['body']);
            });
        }
    }

    public function assertNotSentToZoom(array $assertions): void
    {
        foreach ($assertions as $assert) {
            Http::assertNotSent(static function (Request $request) use ($assert) {
                return str_contains($request->url(), config('zoom.base_url')) &&
                    str_contains($request->url(), $assert['function']) &&
                    $request->method() === ($assert['method'] ?? 'POST') &&
                    (! isset($assert['body']) || $request->body() === $assert['body']);
            });
        }
    }

    public function mockZoomAuth(): void
    {
        Http::fake([
            'https://zoom.us/oauth/token*' => Http::response(
                $this->getZoomResponseContent('auth', 'auth'),
                200,
                ['Content-Type' => 'application/json']
            ),
        ]);
    }

    public function assertZoomAuth(): void
    {
        Http::assertSent(static function (Request $request) {
            return str_contains($request->url(), 'https://zoom.us/oauth/token') &&
                $request->method() === 'POST';
        });
    }

    public function assertNotZoomAuth(): void
    {
        Http::assertNotSent(static function (Request $request) {
            return str_contains($request->url(), 'https://zoom.us/oauth/token') &&
                $request->method() === 'POST';
        });
    }

    public function getZoomResponseContent(string $model, string $function, bool $json_decode = false, bool $associative = false): string|bool|array|object
    {
        $content = sprintf(
            '/../tests/_sample-responses/%s/%s.json',
            $model,
            $function
        );

        $content = file_get_contents(__DIR__.$content);

        if ($json_decode) {
            return json_decode($content, $associative);
        }

        return $content;
    }
}
