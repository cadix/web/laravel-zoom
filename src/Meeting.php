<?php

namespace Cadix\LaravelZoom;

use Exception;

class Meeting extends Model
{
    public string $model = 'meetings';

    /**
     * https://marketplace.zoom.us/docs/api-reference/zoom-api/meetings/meetings
     *
     * @param  string  $user_id
     * @return object|null
     */
    public function all(string $user_id): object|array|null
    {
        $this->client->url = parent::getBaseUrl().'users/'.$user_id.'/meetings';

        return parent::get()->meetings;
    }

    /**
     * https://marketplace.zoom.us/docs/api-reference/zoom-api/meetings/meetings
     *
     * @param  string|null  $user_id
     * @return object|null
     *
     * @throws Exception
     */
    public function get(string|null $user_id = null): object|array|null
    {
        if (! $user_id) {
            throw new Exception('Missing required parameter $user_id');
        }

        $this->client->url = parent::getBaseUrl().'users/'.$user_id.'/meetings';

        return parent::get()->meetings;
    }

    /**
     * https://marketplace.zoom.us/docs/api-reference/zoom-api/meetings/meeting
     *
     * @param  string  $id
     * @return object|null
     */
    public function find(string $id): object|null
    {
        $this->client->url = parent::getBaseUrl().$this->model.'/'.$id;

        return parent::get();
    }

    public function paginate(int $perPage = 10, int $page = 1, int|string $user_id = null): object
    {
        if (! $user_id) {
            throw new \Exception('Required parameter $user_id missing');
        }

        $this->client->url = parent::getBaseUrl().'users/'.$user_id.'/meetings';

        return parent::paginate($perPage, $page);
    }

    /**
     * https://marketplace.zoom.us/docs/api-reference/zoom-api/meetings/meetingcreate
     *
     * @param  string  $user_id
     * @param  array  $body
     * @return object|null
     */
    public function create(string $user_id, array $body): object|null
    {
        $this->client->url = parent::getBaseUrl().'users/'.$user_id.'/meetings';

        return parent::post($body);
    }

    /**
     * https://marketplace.zoom.us/docs/api-reference/zoom-api/meetings/meetingupdate
     *
     * @param  string  $id
     * @param  array  $body
     * @return object|null
     */
    public function update(string $id, array $body): object|null
    {
        $this->client->url = parent::getBaseUrl().$this->model.'/'.$id;

        return parent::patch($body);
    }

    /**
     * https://marketplace.zoom.us/docs/api-reference/zoom-api/meetings/meetingdelete
     *
     * @param  string  $id
     * @param  array|null  $parameters
     * @return bool
     */
    public function delete(string $id, array $parameters = null): bool
    {
        $this->client->url = parent::getBaseUrl().$this->model.'/'.$id;
        $this->client->params = $parameters;

        return parent::destroy($id);
    }
}
