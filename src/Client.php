<?php

namespace Cadix\LaravelZoom;

use Exception;
use GuzzleHttp\Exception\GuzzleException;
use Illuminate\Support\Facades\Http;

class Client
{
    public string|null $url = null;

    /**
     * @var string[]|null
     */
    public array|null $params = null;

    /**
     * @var string[]|null
     */
    public array|null $headers = null;

    public function get(): object|null
    {
        return $this->request('GET', $this->url, $this->headers);
    }

    /**
     * @param  array<string, mixed>|null  $body
     * @return object|null
     *
     * @throws GuzzleException
     */
    public function post(array|null $body = null): object|null
    {
        if (! $body) {
            $body = [];
        }

        return $this->request('POST', $this->url, $this->headers, $body);
    }

    /**
     * @param  array<string, mixed>  $body
     * @return object|null
     *
     * @throws GuzzleException
     */
    public function put(array $body): object|null
    {
        return $this->request('PUT', $this->url, $this->headers, $body);
    }

    /**
     * @param  array<string, mixed>  $body
     * @return object|null
     *
     * @throws GuzzleException
     */
    public function patch(array $body): object|null
    {
        return $this->request('PATCH', $this->url, $this->headers, $body);
    }

    public function delete(): object|null
    {
        return $this->request('DELETE', $this->url, $this->headers);
    }

    /**
     * @param  string  $method
     * @param  string  $url
     * @param  array<string>|null  $headers
     * @param  array<string>  $body
     * @param  int  $tries
     * @return object|null        object|null;
     *
     * @throws GuzzleException
     * @throws Exception
     */
    protected function request(string $method, string $url, array|null $headers = null, array $body = [], int $tries = 0): object|null
    {
        $response = Http::timeout(config('zoom.timeout'))
            ->withOptions([
                'debug' => config('zoom.debug'),
            ]);

        if (is_array($headers) && count($headers) > 0) {
            $response = $response->withHeaders($headers);
        }

        if ($method === 'GET') {
            $response = $response->get($url, $this->params);
        }

        if ($method !== 'GET') {
            if ($this->params) {
                $url .= '?'.urldecode(http_build_query($this->params));
            }

            $response = $response->$method($url, $body);
        }

        if ($response->failed()) {
            if ($tries >= config('zoom.retries')) {
                $message = 'More than '.$tries.' tries for request '.$method.' '.$url.' without success with status '.$response->status().'.';
                if (is_object($response->object()) && ! empty($response->object()?->message)) {
                    $message .= ' Possible reason: '.$response->object()->message;
                }

                throw new Exception($message);
            }

            // Refresh the headers
            if ($response->status() === 401) {
                throw new Exception('Unauthorized response 401');
            }

            if (in_array($response->status(), [403, 404, 409, 429])) {
                // Message must always be a string or other exceptions will be thrown
                abort($response->status(), $response->object()?->message ?? '');
            }

            return $this->request($method, $url, $headers, $body, ++$tries);
        }

        // Reset params after each successful request
        $this->params = null;

        return $response->object();
    }
}
