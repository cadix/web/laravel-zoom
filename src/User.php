<?php

namespace Cadix\LaravelZoom;

class User extends Model
{
    public string $model = 'users';

    /**
     * https://marketplace.zoom.us/docs/api-reference/zoom-api/users/users
     */
    public function all(): array|object|null
    {
        $this->client->url = parent::getBaseUrl().$this->model;

        return parent::get()->users;
    }

    /**
     * https://marketplace.zoom.us/docs/api-reference/zoom-api/users/users
     *
     * @param  string|null  $user_id
     * @return array|null
     */
    public function get(string|null $user_id = null): array|object|null
    {
        $this->client->url = parent::getBaseUrl().$this->model;

        return parent::get()->users;
    }

    /**
     * https://marketplace.zoom.us/docs/api-reference/zoom-api/users/user
     *
     * @param  string  $id
     * @return array|null
     */
    public function find(string $id): object|null
    {
        $this->client->url = parent::getBaseUrl().$this->model.'/'.$id;

        return parent::get();
    }

    /**
     * https://marketplace.zoom.us/docs/api-reference/zoom-api/users/usercreate
     *
     * @param  array  $body
     * @return object|null
     */
    public function create(array $body): object|null
    {
        $this->client->url = parent::getBaseUrl().$this->model;

        return parent::post($body);
    }

    /**
     * https://marketplace.zoom.us/docs/api-reference/zoom-api/users/userupdate
     *
     * @param  string  $id
     * @param  array  $body
     * @return object|null
     */
    public function update(string $id, array $body): object|null
    {
        $this->client->url = parent::getBaseUrl().$this->model.'/'.$id;

        return parent::patch($body);
    }

    /**
     * https://marketplace.zoom.us/docs/api-reference/zoom-api/users/userdelete
     *
     * @param  string  $id
     * @param  array  $parameters
     * @return bool
     */
    public function delete(string $id, array $parameters): bool
    {
        $this->client->url = parent::getBaseUrl().$this->model.'/'.$id;
        $this->client->params = $parameters;

        return parent::destroy($id);
    }
}
