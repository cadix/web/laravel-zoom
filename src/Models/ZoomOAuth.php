<?php

namespace Cadix\LaravelZoom\Models;

use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * Cadix\LaravelZoom\Models\ZoomOAuth
 *
 * @property string $access_token
 * @property string $token_type
 * @property string $expires_at
 *
 * @method static \Cadix\LaravelZoom\Database\Factories\ZoomOAuthFactory factory(...$parameters)
 * @method static \Illuminate\Database\Eloquent\Builder|ZoomOAuth        newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|ZoomOAuth        newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|ZoomOAuth        query()
 * @mixin Model
 */
class ZoomOAuth extends Model
{
    use HasFactory;

    public $timestamps = false;

    protected $table = 'zoom_oauths';

    protected $fillable = [
        'access_token',
        'token_type',
        'expires_at',
    ];

    protected static function newFactory(): Factory
    {
        return \Cadix\LaravelZoom\Database\Factories\ZoomOAuthFactory::new();
    }
}
