<?php

namespace Cadix\LaravelZoom\Http\Controllers;

use Cadix\LaravelZoom\Events\Meeting\Alert as MeetingAlert;
use Cadix\LaravelZoom\Events\Meeting\Created as MeetingCreated;
use Cadix\LaravelZoom\Events\Meeting\Deleted as MeetingDeleted;
use Cadix\LaravelZoom\Events\Meeting\Ended as MeetingEnded;
use Cadix\LaravelZoom\Events\Meeting\ParticipantJoined as MeetingParticipantJoined;
use Cadix\LaravelZoom\Events\Meeting\ParticipantLeft as MeetingParticipantLeft;
use Cadix\LaravelZoom\Events\Meeting\Started as MeetingStarted;
use Cadix\LaravelZoom\Events\Meeting\Updated as MeetingUpdated;
use Cadix\LaravelZoom\Events\User\Created as UserCreated;
use Cadix\LaravelZoom\Events\User\Deactivated as UserDeactivated;
use Cadix\LaravelZoom\Events\User\Deleted as UserDeleted;
use Cadix\LaravelZoom\Events\User\SignedIn as UserSignedIn;
use Cadix\LaravelZoom\Events\User\SignedOut as UserSignedOut;
use Cadix\LaravelZoom\Events\User\Updated as UserUpdated;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class WebhookController extends Controller
{
    /**
     * @param  Request  $request
     * @return array<mixed>|JsonResponse|null
     */
    public function __invoke(Request $request): array|JsonResponse|null
    {
        return match ($request->event) {
            'endpoint.url_validation' => response()->json([
                'plainToken'     => $request->payload['plainToken'],
                'encryptedToken' => hash_hmac('sha256', $request->payload['plainToken'], config('zoom.secret_token')),
            ]),

            'meeting.alert'              => event(new MeetingAlert($request)),
            'meeting.created'            => event(new MeetingCreated($request)),
            'meeting.deleted'            => event(new MeetingDeleted($request)),
            'meeting.ended'              => event(new MeetingEnded($request)),
            'meeting.participant_joined' => event(new MeetingParticipantJoined($request)),
            'meeting.participant_left'   => event(new MeetingParticipantLeft($request)),
            'meeting.started'            => event(new MeetingStarted($request)),
            'meeting.updated'            => event(new MeetingUpdated($request)),

            'user.created'     => event(new UserCreated($request)),
            'user.deactivated' => event(new UserDeactivated($request)),
            'user.deleted'     => event(new UserDeleted($request)),
            'user.signed_in'   => event(new UserSignedIn($request)),
            'user.signed_out'  => event(new UserSignedOut($request)),
            'user.updated'     => event(new UserUpdated($request)),

            default => null,
        };
    }
}
