<?php

return [
    'base_url'      => env('ZOOM_BASE_URL'),
    'secret_token'  => env('ZOOM_SECRET_TOKEN'),
    'account_id'    => env('ZOOM_ACCOUNT_ID'),
    'client_id'     => env('ZOOM_CLIENT_ID'),
    'client_secret' => env('ZOOM_CLIENT_SECRET'),

    'retries' => env('ZOOM_RETRIES', 3),
    'timeout' => env('ZOOM_TIMEOUT', 2), // in seconds

    'debug' => env('ZOOM_DEBUG', false),
];
