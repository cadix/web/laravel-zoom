<?php

namespace Cadix\LaravelZoom\Tests\Feature\Webhook;

use Cadix\LaravelZoom\Events\Meeting\Alert as MeetingAlert;
use Cadix\LaravelZoom\Events\Meeting\Created as MeetingCreated;
use Cadix\LaravelZoom\Events\Meeting\Deleted as MeetingDeleted;
use Cadix\LaravelZoom\Events\Meeting\Ended as MeetingEnded;
use Cadix\LaravelZoom\Events\Meeting\ParticipantJoined as MeetingParticipantJoined;
use Cadix\LaravelZoom\Events\Meeting\ParticipantLeft as MeetingParticipantLeft;
use Cadix\LaravelZoom\Events\Meeting\Started as MeetingStarted;
use Cadix\LaravelZoom\Events\Meeting\Updated as MeetingUpdated;
use Cadix\LaravelZoom\Events\User\Created as UserCreated;
use Cadix\LaravelZoom\Events\User\Deactivated as UserDeactivated;
use Cadix\LaravelZoom\Events\User\Deleted as UserDeleted;
use Cadix\LaravelZoom\Events\User\SignedIn as UserSignedIn;
use Cadix\LaravelZoom\Events\User\SignedOut as UserSignedOut;
use Cadix\LaravelZoom\Events\User\Updated as UserUpdated;
use Cadix\LaravelZoom\Tests\TestCase;
use Generator;
use Illuminate\Support\Facades\Event;
use Illuminate\Testing\Fluent\AssertableJson;

class EventsTest extends TestCase
{
    /**
     * @dataProvider webhookEventsProvider
     *
     * @param  string  $event_class
     * @param  string  $event_name
     * @return void
     */
    public function test_it_triggers_events(string $event_class, string $event_name): void
    {
        Event::fake();

        $content = $this->getZoomResponseContent('webhooks', $event_name, true, true);
        $response = $this->postJson(
            route('zoom.webhooks.handle'),
            $content,
            [
                'x-zm-signature' => config('zoom.secret_token'),
            ]
        );

        Event::assertDispatched($event_class, static function ($event) use ($content) {
            return $event->body->event === $content['event'];
        });
        $response->assertSuccessful();
    }

    /**
     * @return Generator<string, array<int, string>>
     */
    public static function webhookEventsProvider(): Generator
    {
        yield 'it triggers MeetingAlert event' => [MeetingAlert::class, 'meeting.alert'];
        yield 'it triggers MeetingCreated event' => [MeetingCreated::class, 'meeting.created'];
        yield 'it triggers MeetingDeleted event' => [MeetingDeleted::class, 'meeting.deleted'];
        yield 'it triggers MeetingEnded event' => [MeetingEnded::class, 'meeting.ended'];
        yield 'it triggers MeetingParticipantJoined event' => [MeetingParticipantJoined::class, 'meeting.participant_joined'];
        yield 'it triggers MeetingParticipantLeft event' => [MeetingParticipantLeft::class, 'meeting.participant_left'];
        yield 'it triggers MeetingStarted event' => [MeetingStarted::class, 'meeting.started'];
        yield 'it triggers MeetingUpdated event' => [MeetingUpdated::class, 'meeting.updated'];

        yield 'it triggers UserCreated event' => [UserCreated::class, 'user.created'];
        yield 'it triggers UserDeactivated event' => [UserDeactivated::class, 'user.deactivated'];
        yield 'it triggers UserDeleted event' => [UserDeleted::class, 'user.deleted'];
        yield 'it triggers UserSignedIn event' => [UserSignedIn::class, 'user.signed_in'];
        yield 'it triggers UserSignedOut event' => [UserSignedOut::class, 'user.signed_out'];
        yield 'it triggers UserUpdated event' => [UserUpdated::class, 'user.updated'];
    }

    public function test_it_will_validate_webhook_endpoint(): void
    {
        $content = $this->getZoomResponseContent('webhooks', 'endpoint.url_validation', true);
        $response = $this->postJson(
            route('zoom.webhooks.handle'),
            $content,
            [
                'authorization' => config('zoom.secret_token'),
            ]
        );

        $response->assertSuccessful()
            ->assertJson(function(AssertableJson $json) use($content) {
                $json->where('plainToken', $content->plainToken)
                    ->where('encryptedToken', hash_hmac('sha256', $content->plainToken, config('zoom.secret_token')));
            });
    }
}
