<?php

namespace Cadix\LaravelZoom\Tests\Feature\Webhook;

use Cadix\LaravelZoom\Events\Meeting\Alert;
use Cadix\LaravelZoom\Tests\TestCase;
use Illuminate\Support\Facades\Event;

class MiddlewareTest extends TestCase
{
    public function test_as_a_guest_i_cannot_trigger_a_webhook(): void
    {
        Event::fake();

        $content = $this->getZoomResponseContent('webhooks', 'meeting.alert', true, true);
        $response = $this->postJson(route('zoom.webhooks.handle'), $content);

        $response->assertForbidden();

        Event::assertNotDispatched(Alert::class);
    }

    public function test_as_a_webhook_my_authorization_token_must_be_valid(): void
    {
        Event::fake();

        $content = $this->getZoomResponseContent('webhooks', 'meeting.alert', true, true);
        $response = $this->postJson(
            route('zoom.webhooks.handle'),
            $content,
            [
                'authorization' => 'invalid-token',
            ]
        );

        $response->assertForbidden();

        Event::assertNotDispatched(Alert::class);
    }

    public function test_as_a_webhook_i_trigger_an_event_if_the_token_is_valid(): void
    {
        Event::fake();

        $content = $this->getZoomResponseContent('webhooks', 'meeting.alert', true, true);
        $response = $this->postJson(
            route('zoom.webhooks.handle'),
            $content,
            [
                'authorization' => config('zoom.secret_token'),
            ]
        );

        $response->assertSuccessful();

        Event::assertDispatched(Alert::class);
    }
}
