<?php

namespace Cadix\LaravelZoom\Tests\Feature;

use Cadix\LaravelZoom\Facades\User;
use Cadix\LaravelZoom\Models\ZoomOAuth;
use Cadix\LaravelZoom\Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Support\Facades\Http;

class UserTest extends TestCase
{
    use WithFaker;

    public function test_it_can_get_all_users(): void
    {
        ZoomOauth::factory()->create();
        $this->mockZoomRequest([['model' => 'users', 'function' => 'index']]);

        $users = User::all();

        $this->assertIsArray($users);

        $this->assertSentToZoom([['method' => 'GET', 'function' => 'users']]);
        $this->assertNotZoomAuth();
    }

    public function test_it_can_get_users(): void
    {
        ZoomOauth::factory()->create();
        $this->mockZoomRequest([['model' => 'users', 'function' => 'index']]);

        $users = User::get();

        $this->assertIsArray($users);

        $this->assertSentToZoom([['method' => 'GET', 'function' => 'users']]);
        $this->assertNotZoomAuth();
    }

    public function test_it_can_find_users(): void
    {
        ZoomOauth::factory()->create();
        $this->mockZoomRequest([['model' => 'users', 'function' => 'find']]);

        $id = 'z8dsdsdsdsdCfp8uQ';
        $user = User::find($id);

        $this->assertIsObject($user);
        $this->assertEquals($id, $user->id);

        $this->assertSentToZoom([['method' => 'GET', 'function' => 'users/'.$id]]);
        $this->assertNotZoomAuth();
    }

    public function test_it_can_paginate_users(): void
    {
        ZoomOauth::factory()->create();
        $this->mockZoomRequest([['model' => 'users', 'function' => 'index']]);

        $per_page = 30;
        $page = 1;
        $users = User::paginate($per_page, $page);

        $this->assertIsObject($users);
        $this->assertSame($users->total, 23);
        $this->assertSame($users->per_page, $per_page);
        $this->assertSame($users->current_page, $page);
        $this->assertSame($users->last_page, 1);
        $this->assertSame($users->from, 1);
        $this->assertSame($users->to, $per_page);
        $this->assertIsArray($users->data);

        $this->assertSentToZoom([['method' => 'GET', 'function' => 'users?page_size='.$per_page.'&page_number='.$page]]);
        $this->assertNotZoomAuth();
    }

    public function test_it_can_create_users(): void
    {
        ZoomOauth::factory()->create();
        Http::fake([
            config('zoom.base_url').'*' => Http::response(null, 204),
        ]);

        $user = [
            'action'    => 'create',
            'user_info' => [
                'email'      => $this->faker->safeEmail,
                'type'       => 1,
                'first_name' => $this->faker->firstName,
                'last_name'  => $this->faker->lastName,
            ],
        ];

        $response = User::create($user);

        $this->assertNull($response);

        $this->assertSentToZoom([['method' => 'POST', 'function' => 'users', 'body' => json_encode($user)]]);
        $this->assertNotZoomAuth();
    }

    public function test_it_can_update_users(): void
    {
        ZoomOauth::factory()->create();
        Http::fake([
            config('zoom.base_url').'*' => Http::response(null, 204),
        ]);

        $user = [
            'first_name' => $this->faker->firstName,
            'last_name'  => $this->faker->lastName,
            'type'       => 2,
        ];

        $id = '34r3f34f';
        $response = User::update($id, $user);

        $this->assertNull($response);

        $this->assertSentToZoom([['method' => 'PATCH', 'function' => 'users/'.$id, 'body' => json_encode($user)]]);
        $this->assertNotZoomAuth();
    }

    public function test_it_can_delete_users(): void
    {
        ZoomOauth::factory()->create();
        Http::fake([
            config('zoom.base_url').'*' => Http::response(null, 204),
        ]);

        $id = '34r3f34f';
        $parameters = [
            'action' => 'delete',
        ];
        $response = User::delete($id, $parameters);

        $this->assertTrue($response);

        $this->assertSentToZoom([['method' => 'DELETE', 'function' => 'users/'.$id.'?action='.$parameters['action']]]);
        $this->assertNotZoomAuth();
    }
}
