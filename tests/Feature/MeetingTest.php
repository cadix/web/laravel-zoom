<?php

namespace Cadix\LaravelZoom\Tests\Feature;

use Cadix\LaravelZoom\Facades\Meeting;
use Cadix\LaravelZoom\Models\ZoomOAuth;
use Cadix\LaravelZoom\Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Support\Facades\Http;

class MeetingTest extends TestCase
{
    use WithFaker;

    public function test_it_can_get_all_meetings_from_a_user(): void
    {
        $user_id = 'c43ff';

        ZoomOauth::factory()->create();
        $this->mockZoomRequest([['model' => 'meetings', 'function' => 'index']]);

        $meetings = Meeting::all($user_id);
        $this->assertIsArray($meetings);

        $this->assertSentToZoom([['method' => 'GET', 'function' => 'users/'.$user_id.'/meetings']]);
        $this->assertNotZoomAuth();
    }

    public function test_it_can_get_meetings_from_a_user(): void
    {
        $user_id = 'c43ff';

        ZoomOauth::factory()->create();
        $this->mockZoomRequest([['model' => 'meetings', 'function' => 'index']]);

        $meetings = Meeting::get($user_id);
        $this->assertIsArray($meetings);

        $this->assertSentToZoom([['method' => 'GET', 'function' => 'users/'.$user_id.'/meetings']]);
        $this->assertNotZoomAuth();
    }

    public function test_it_can_find_meetings(): void
    {
        ZoomOauth::factory()->create();
        $this->mockZoomRequest([['model' => 'meetings', 'function' => 'find']]);

        $id = 1234555466;
        $meeting = Meeting::find($id);
        $this->assertIsObject($meeting);
        $this->assertSame($id, $meeting->id);

        $this->assertSentToZoom([['method' => 'GET', 'function' => 'meetings/'.$id]]);

        $this->assertNotZoomAuth();
    }

    public function test_it_can_paginate_meetings(): void
    {
        ZoomOauth::factory()->create();
        $this->mockZoomRequest([['model' => 'meetings', 'function' => 'index']]);

        $user_id = 'c43ff';
        $per_page = 30;
        $page = 1;
        $meetings = Meeting::paginate($per_page, $page, $user_id);

        $this->assertIsObject($meetings);
        $this->assertSame($meetings->total, 4);
        $this->assertSame($meetings->per_page, $per_page);
        $this->assertSame($meetings->current_page, $page);
        $this->assertSame($meetings->last_page, 1);
        $this->assertSame($meetings->from, 1);
        $this->assertSame($meetings->to, $per_page);
        $this->assertIsArray($meetings->data);

        $this->assertSentToZoom([['method' => 'GET', 'function' => 'users/'.$user_id.'/meetings?page_size='.$per_page.'&page_number='.$page]]);
        $this->assertNotZoomAuth();
    }

    public function test_it_can_create_meetings(): void
    {
        ZoomOauth::factory()->create();
        Http::fake([
            config('zoom.base_url').'*' => Http::response(null, 201),
        ]);

        $user_id = 'c43ff';
        $meeting = [
            'topic'      => $this->faker->realText,
            'type'       => 2,
            'start_time' => $this->faker->date('Y-m-d\TH:i:s'),
            'duration'   => random_int(2, 8),
            'timezone'   => $this->faker->timezone,
        ];

        Meeting::create($user_id, $meeting);

        $this->assertSentToZoom([['method' => 'POST', 'function' => 'users/'.$user_id.'/meetings', 'body' => json_encode($meeting)]]);
        $this->assertNotZoomAuth();
    }

    public function test_it_can_update_meetings(): void
    {
        ZoomOauth::factory()->create();
        Http::fake([
            config('zoom.base_url').'*' => Http::response(null, 204),
        ]);

        $meeting_id = 'f43qwf4';
        $meeting = [
            'topic'      => $this->faker->realText,
            'type'       => 2,
            'start_time' => $this->faker->date('Y-m-d\TH:i:s'),
            'duration'   => random_int(2, 8),
            'timezone'   => $this->faker->timezone,
        ];

        Meeting::update($meeting_id, $meeting);

        $this->assertSentToZoom([['method' => 'PATCH', 'function' => 'meetings/'.$meeting_id, 'body' => json_encode($meeting)]]);

        $this->assertNotZoomAuth();
    }

    public function test_it_can_delete_meetings(): void
    {
        ZoomOauth::factory()->create();
        Http::fake([
            config('zoom.base_url').'*' => Http::response(null, 204),
        ]);

        $meeting_id = 'f43qwf4';

        Meeting::delete($meeting_id);

        $this->assertSentToZoom([['method' => 'DELETE', 'function' => 'meetings/'.$meeting_id]]);

        $this->assertNotZoomAuth();
    }
}
