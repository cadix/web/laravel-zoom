<?php

namespace Cadix\LaravelZoom\Tests\Feature;

use Cadix\LaravelZoom\Facades\User;
use Cadix\LaravelZoom\Models\ZoomOAuth;
use Cadix\LaravelZoom\Tests\TestCase;

class AuthTest extends TestCase
{
    public function test_it_will_store_an_access_token_if_none_is_created(): void
    {
        $this->mockZoomRequest([['model' => 'users', 'function' => 'index']]);
        $this->mockZoomAuth();

        $content = $this->getZoomResponseContent('auth', 'auth', true);

        $users = User::all();

        $this->assertIsArray($users);

        $this->assertSentToZoom([['method' => 'GET', 'function' => 'users']]);
        $this->assertZoomAuth();

        $this->assertDatabaseHas(( new ZoomOAuth() )->getTable(), [
            'access_token' => $content->access_token,
            'token_type'   => 'Bearer',
            'expires_at'   => now()->addSeconds($content->expires_in)->toDateTimeString(),
        ]);
    }

    public function test_it_will_update_an_access_token_if_it_is_expired(): void
    {
        $time = now()->subHour()->toDateTimeString();
        $token = ZoomOAuth::factory()->create(['expires_at' => $time]);
        $this->assertDatabaseHas(( new ZoomOAuth() )->getTable(), [
            'token_type' => 'Bearer',
            'expires_at' => $time,
        ]);

        $this->mockZoomRequest([['model' => 'users', 'function' => 'index']]);
        $this->mockZoomAuth();

        $content = $this->getZoomResponseContent('auth', 'auth', true);

        $users = User::all();

        $this->assertIsArray($users);

        $this->assertSentToZoom([['method' => 'GET', 'function' => 'users']]);
        $this->assertZoomAuth();

        $this->assertDatabaseHas(( new ZoomOAuth() )->getTable(), [
            'id'           => $token->id,
            'access_token' => $content->access_token,
            'token_type'   => 'Bearer',
            'expires_at'   => now()->addSeconds($content->expires_in)->toDateTimeString(),
        ]);
    }

    public function test_it_will_not_update_an_access_token_if_it_is_still_valid(): void
    {
        $time = now()->addHours(2)->toDateTimeString();
        $token = ZoomOAuth::factory()->create(['expires_at' => $time]);
        $this->assertDatabaseHas(( new ZoomOAuth() )->getTable(), [
            'token_type' => 'Bearer',
            'expires_at' => $time,
        ]);

        $this->mockZoomRequest([['model' => 'users', 'function' => 'index']]);
        $this->mockZoomAuth();

        $content = $this->getZoomResponseContent('auth', 'auth', true);

        $users = User::all();

        $this->assertIsArray($users);

        $this->assertSentToZoom([['method' => 'GET', 'function' => 'users']]);
        $this->assertNotZoomAuth();

        $this->assertDatabaseHas(( new ZoomOAuth() )->getTable(), [
            'id'           => $token->id,
            'access_token' => $token->access_token,
            'token_type'   => 'Bearer',
            'expires_at'   => $time,
        ]);
    }
}
