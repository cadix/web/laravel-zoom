<?php

namespace Cadix\LaravelZoom\Tests;

use Cadix\LaravelZoom\LaravelZoomServiceProvider;
use Cadix\LaravelZoom\ZoomApplication;
use Illuminate\Foundation\Bootstrap\LoadEnvironmentVariables;
use Illuminate\Support\Facades\Http;

class TestCase extends \Orchestra\Testbench\TestCase
{
    use ZoomApplication;

    public function setUp(): void
    {
        parent::setUp();

        Http::preventStrayRequests();

        $this->local = true;
    }

    protected function getPackageProviders($app): array
    {
        return [
            LaravelZoomServiceProvider::class,
        ];
    }

    protected function getEnvironmentSetUp($app): void
    {
        // make sure, our .env file is loaded
        $app->useEnvironmentPath(__DIR__.'/..');
        $app->bootstrapWith([LoadEnvironmentVariables::class]);
        parent::getEnvironmentSetUp($app);
        $app['config']->set('zoom.base_url', env('ZOOM_BASE_URL'));
        $app['config']->set('zoom.timeout', env('ZOOM_TIMEOUT'));
        $app['config']->set('zoom.secret_token', env('ZOOM_SECRET_TOKEN'));
        $app['config']->set('zoom.account_id', env('ZOOM_ACCOUNT_ID'));
        $app['config']->set('zoom.client_id', env('ZOOM_CLIENT_ID'));
        $app['config']->set('zoom.client_secret', env('ZOOM_CLIENT_SECRET'));
        $app['config']->set('zoom.retires', env('ZOOM_RETRIES'));
        $app['config']->set('zoom.timeout', env('ZOOM_TIMEOUT'));

        include_once __DIR__.'/../database/migrations/2022_11_25_100000_create_zoom_oauths_table.php';
        ( new \CreateZoomOAuthsTable() )->up();
    }
}
